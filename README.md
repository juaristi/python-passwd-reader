# Usage

```
./passwd.py /etc/passwd
```

It's written for Python 3, though it works in Python 2 as well,
with some minor output formatting differences.