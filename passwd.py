#!/usr/bin/env python3
# vim: set ts=4 sw=4 sts=4 si et :
import sys
import os.path

class PasswdSyntaxError(Exception):
    def __init__(self, line_number):
        self.line_number = line_number

class PasswdReader:
    def __init__(self, filename):
        self.filename = filename

    def read_shells(self, lmd):
        with open(self.filename, "r") as f:
            lineno = 1

            for line in f:
                try:
                    idx_user, idx_shell = line.index(':'), line.rindex(':')

                    # Some basic sanity checks
                    if idx_user >= idx_shell:
                        raise PasswdSyntaxError(lineno)

                    user, shell = line[:idx_user].strip(), line[idx_shell + 1:].strip()

                    if len(user) == 0 or len(shell) == 0:
                        raise PasswdSyntaxError(lineno)

                    # Call user-supplied lambda
                    lmd(user, shell)
                    lineno += 1
                except ValueError:
                    raise PasswdSyntaxError(lineno)

class ShellCounter:
    def __init__(self):
        self.shells = {}

    def add_shell(self, user, shell):
        if shell in self.shells:
            self.shells[shell].add(user)
        else:
            self.shells[shell] = { user }

    def print_stats(self):
        print("Shells:")
        print()

        largest = 0

        for k in self.shells:
            l = len(k)
            if l > largest:
                largest = l

        for k, v in self.shells.items():
            spaces = (largest + 4) - len(k)
            print(k, " " * spaces, "{0} users".format(len(v)))

try:
    passwd_file = sys.argv[1]

    pwr = PasswdReader(passwd_file)
    sc = ShellCounter()

    # pwr.read_shells(lambda user, shell: print(user, shell))
    pwr.read_shells(lambda user, shell: sc.add_shell(user, shell))

    sc.print_stats()
except PasswdSyntaxError as se:
    print("Passwd file '{0}' malformed: at line {1}".format(passwd_file, se.line_number))
except IndexError:
    print("Usage: {0} <file path>".format(os.path.basename(__file__)))
    sys.exit(1)
except FileNotFoundError:
    print("Could not open file '{0}' (file does not exist)".format(passwd_file))
    sys.exit(1)
except PermissionError:
    print("Could not open file '{0}' (permission denied)".format(passwd_file))
    sys.exit(1)
except IsADirectoryError:
    print("Could not open file'{0}' (it is a directory)".format(passwd_file))
    sys.exit(1)

